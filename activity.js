db.course.insertMany([

    {

        "name": "HTML Basics",

        "price": 20000,

        "isActive":true,

        "instructor":"Sir Alvin"

    

    },

       {

        "name": "CSS 101 + Flexbox",

        "price": 21000,

        "isActive":true,

        "instructor":"Sir Alvin"

    

    },

         {

        "name": "Javascript",

        "price": 32000,

        "isActive":true,

        "instructor":"Ma'am Tine"

    

    },

     {

        "name": "Git 101, IDE and CLI",

        "price": 19000,

        "isActive":false,

        "instructor":"Ma'am Tine"

      

    },

     {

        "name": "React.js 101",

        "price": 25000,

        "isActive":true,

        "instructor":"Ma'am Miah"

    

    }, 

    ])


db.course.find({$or:[{instructor:{$regex: 'Sir Alvin',$options: '$i'}},{price:{$gte:20000}}]})
db.course.find({$and:[{instructor:{$regex: 'Sir Alvin',$options: '$i'}},{price:{$gte:20000}}]})
db.course.find({$and:[{instructor:{$regex: "Ma'am Tine",$options: '$i'}},{isActive:true}]})
db.course.find({$or:[{name:{$regex: 'r',$options:'$i'}},{price:{lte:25000}}]})
db.course.updateMany({price:{$lt:21000}},{$set:{"isActive":false}})
db.course.deleteMany({price:{$gte:25000}})